# **Swadeli**

## **HTTP Success and Error Codes**

this codes will be implemented in our system are linked below.

### **HTTP Success Codes**

| Message | CODE |
| ------- | ---- |
| OK      | 200  |
| Created | 201  |

### **HTTP Error Codes**

| Message                                   | CODE |
| ----------------------------------------- | ---- |
| Bad Request                               | 400  |
| Unauthorize User                          | 401  |
| Request Access Forbidden OR Token Expired | 403  |
| Not Found                                 | 404  |
| Internal Server Error                     | 500  |

## **Message Base From BE**

this Message will be implemented in our system are linked below.

| Message         | CODE            |
| --------------- | --------------- |
| NOT IMPLEMENTED | NOT IMPLEMENTED |

## **Accessing BE docker images**

```sh
ssh root@ip_server
```

```sh
ENTER YOUR PASS
```

check docker images

```sh
docker images
```

```sh
docker ps -a
```

```sh
docker volume list
```
