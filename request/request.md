# **Swadeli**

## **Request Templating**

this template will be implemented in our request system are linked below.

### **Basic Authentication**

authorization will be use in **admin** and **mobile** for basic authentication.
with

```sh
'Authorization: Bearer our-jwt-token'
```

example

```sh
curl --location --request GET 'https://dev.swadeli.id/some-long-url' \
--header 'Authorization: Bearer our-jwt-token'
```

### **Refreshing token -- Authentication**

refreshing token will be use in **admin** and **mobile** for basic refresh token when token is expired.
with request

```sh
curl --location --request POST 'https://dev.swadeli.id/v1/auth/refresh-token' \
--header 'Content-Type: application/json' \
--data-raw '{
    "refresh_token":"some token here"
}'
```

and response will be

```json
{
  "code": 200, // integer,
  "msg": ["ok"],
  "data": {
    "token": "this is new token", // new token
    "token_expires_in": 1440, // in minute
    "refresh_token": "this is new refresh_token", // new refresh_token
    "refresh_token_expires_in": 1440 // in minute
  }
}
```

**NOTE :** url can be changed sometimes, pleas contact BE developer

### **Cursor Pagination**

this methode will be implement in **mobile** for faster query. with using date and last id of list.

#### **cursor pagination first page**

this is for first page. **no need** for import last id and last date
ex required query params
| Query Params | value |
| ------ | ------ |
| order | DESC |
| filter | last_name |
| page | 1 |
| size | 10 |
result url

```sh
curl --location --request GET
https://dev.swadeli.id/products?order=DESC&filter=last_name&page=1&size=10
```

#### **cursor pagination second page to end**

this is for second page third, forth to end. **required** for import last id and last date. so BE can query the data faster.
ex required query params
| Query Params | value |
| ------ | ------ |
|last-id|309a84a0-7ec6-11ec-88b5-9303c13b155b |
|last-date|2022-01-11 |
| order | DESC |
| filter | last_name |
| page | 1 |
| size | 10 |
result url

```sh
curl --location --request GET
https://dev.swadeli.id/products?last-id=309a84a0-7ec6-11ec-88b5-9303c13b155b&last-date=2022-01-11&order=DESC&filter=last_name&page=2&size=10
```

#### **Basic Pagination**

this methode will be implement in **admin**. with using page and and total-data of list.
**required** for import the query below.
ex query params
| Query Params | value |
| ------ | ------ |
| order | DESC |
| filter | last_name |
| page | 1 |
| size | 10 |
result url

```sh
curl --location --request GET
https://dev.swadeli.id/products?last-date=2022-01-11&order=DESC&filter=last_name&page=2&size=10
```
