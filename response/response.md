# **Swadeli**

## **Response Templating**

this template will be implemented in our response system are linked below. the detail of **response** will be in **swagger file**.

#### **example error message 1**

```json
{
  "code": 400, // integer,
  "msg": ["email is required", "full_name is required"], // error messages
  "data": null //
}
```

#### **example success message 1 - single data**

```json
{
  "code": 200, // integer,
  "msg": ["ok"], // success messages
  "data": {
    "id": "fa5b9f69-5c0a-4306-8a1a-65dd254ca9f4",
    "username": "kim_jong_un",
    "email": "kim_jong_un@supremeleader.kor",
    "addresses": [
      {
        "street": "restricted area",
        "lat": 41.40338,
        "long": 2.17403
      }
    ]
  }
}
```

#### **example success message 2 - multiple data / array / pagination**

```json
{
  "code": 200, // integer,
  "msg": ["ok"], // success messages
  "data": {
    "records": [
      {
        "id": 1,
        "role": "user"
      },
      {
        "id": 2,
        "role": "superadmin"
      } // and the rest of data ....
    ],
    "total_data": 120,
    "total_page": 12,
    "per_page": 10
  }
}
```
